Package: imlib2
Version: 1.4.6
Revision: 2
Description: Image handling library for X11
License: BSD
# Free to take over
Maintainer: Hanspeter Niederstrasser <nieder@users.sourceforge.net>

Depends: <<
	%N-shlibs (= %v-%r),
	freetype219-shlibs (>= 2.4.11-1),
	giflib-shlibs,
	x11-shlibs
<<
BuildDepends: <<
	bzip2-dev,
	fink (>= 0.28-1),
	fink-package-precedence,
	freetype219 (>= 2.4.11-1),
	giflib,
	libid3tag,
	libjpeg9,
	libpng16,
	libtiff5,
	libtool2,
	pkgconfig (>= 0.23),
	x11-dev
<<
Replaces: %N-shlibs (<< 1.1.0-14)
BuildDependsOnly: true

Source: mirror:sourceforge:enlightenment/%n-%v.tar.bz2
Source-MD5: 5c7104121ec6db652b37f74a6d7048e2
PatchFile: %n.patch
PatchFile-MD5: 9961a9afaff656153b114ed82f20349b
PatchScript: <<
	%{default_script}
	# imlib2-config has the macro '@my_libs@', but there is no code anywhere to replace it
	# Fixed upstream: 5dde234b2d3caf067ea827858c53adc5d4c56c13
	perl -pi -e 's|\@my_libs\@||g' imlib2-config.in
<<

ConfigureParams: <<
	--x-includes=/usr/X11R6/include \
	--x-libraries=/usr/X11R6/lib \
	--enable-visibility-hiding \
	--enable-dependency-tracking \
	--disable-static \
	--disable-amd64
<<

CompileScript: <<
#!/bin/sh -ev
	./configure %c
	make -w V=1
	fink-package-precedence --prohibit-bdep=%n .
<<
InfoTest: <<
	TestScript: make check || exit 2
<<
InstallScript: 	make install DESTDIR=%d
DocFiles: AUTHORS COPYING ChangeLog README TODO doc/*.html doc/*.gif
SplitOff: <<
	Package: %N-shlibs
	Depends: <<
		bzip2-shlibs,
		freetype219-shlibs (>= 2.4.11-1),
		giflib-shlibs,
		libid3tag-shlibs,
		libjpeg9-shlibs,
		libpng16-shlibs,
		libtiff5-shlibs,
		x11
	<<
	DocFiles: COPYING
	Files: lib/libImlib2.*.dylib lib/imlib2
	Shlibs: %p/lib/libImlib2.1.dylib 6.0.0 %n (>= 1.4.0-1)
<<

#Homepage: http://enlightenment.sourceforge.net/Libraries/Imlib2/
Homepage: http://docs.enlightenment.org/api/imlib2/html/
DescDetail: <<
Imlib2 is the successor to Imlib.  It is not just a newer version, it is a
completely new library.  Imlib2 can be installed alongside Imlib 1.x
without any problems since they are effectively different libraries, but
they have very similar functionality.
<<
DescPackaging: <<
For version 1.4.5
-----------------
Move setting -no-undefined to PatchFile to guarantee catching changes
when upstream modifies files.

amd64 opts broken on 10.6/x86_64. Build on 10.7, but symbols are incorrect
when linking object files into the dylib, so keep it disabled.

Previously maintained by Benjamin Reed <imlib2@fink.raccoonfink.com>
<<
