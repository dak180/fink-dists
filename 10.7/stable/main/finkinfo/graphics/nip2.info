Package: nip2
Version: 7.40.4
Revision: 1
Description: Graphical image manipulation tool
License: LGPL
# Free to update, modify and take over
Maintainer: Hanspeter Niederstrasser <nieder@users.sourceforge.net>
Depends: <<
	atk1-shlibs (>= 1.32.0-1),
	cairo-shlibs (>= 1.12.14-1),
	desktop-file-utils,
	fftw3-shlibs (>= 3.1.2-1),
	fontconfig2-shlibs (>= 2.10.0-1),
	freetype219-shlibs (>= 2.4.11-1),
	glib2-shlibs (>= 2.22.4-5),
	gsl-shlibs,
	gtk+2-shlibs (>= 2.18.9-11),
	libgettext8-shlibs,
	libgsf1.115-shlibs,
	libvips38-shlibs,
	libxml2-shlibs (>= 2.9.1-1),
	pango1-xft2-ft219-shlibs (>= 1.24.5-4),
	shared-mime-info
<<
BuildDepends: <<
	atk1 (>= 1.32.0-1),
	cairo (>= 1.12.14-1),
	fftw3 (>= 3.1.2-1),
	fink-package-precedence,
	fontconfig2-dev (>= 2.10.0-1),
	freetype219 (>= 2.4.11-1),
	gettext-tools,
	glib2-dev (>= 2.22.4-5),
	gsl,
	gtk+2-dev (>= 2.18.9-11),
	libgettext8-dev,
	libgsf1.115-dev,
	libiconv-dev,
	libvips38-dev,
	libxml2 (>= 2.9.1-1),
	pango1-xft2-ft219-dev (>= 1.24.5-4),
	pkgconfig,
	shared-mime-info,
	x11-dev
<<
Source: http://www.vips.ecs.soton.ac.uk/supported/7.40/%n-%v.tar.gz
Source-MD5: 4aba553c6fde61925326252334f9dd83
Source-Checksum: SHA1(e973f4143abfe6f99de94600a2c3c72abd57b9ca)
ConfigureParams: <<
	--mandir=%p/share/man \
	--enable-dependency-tracking \
	--without-libgoffice \
	--without-libgvc \
	--disable-update-desktop
<<
GCC: 4.0
PatchFile: %n.patch
PatchFile-MD5: 307d8d77504b1b7795a4416a02a9db15
PatchScript: <<
	# directly pass detected fftw linker flags (formerly inherited
	# from libvips, which had no reason to publish them)
	%{default_script}

	# fink doesn't have gtk >= 2.22 yet, make sure
	# we get consistent build results when it does
	perl -pi -e 's/(gtk\+-2.0 >=) 2\.20/${1} >= 99999.FORCE.NONDETECT/g' configure
	perl -pi -e 's/(gtk\+-2.0 >=) 2\.24/${1} >= 99999.FORCE.NONDETECT/g' configure
<<
CompileScript: <<
	%{default_script}
	fink-package-precedence .
<<
InstallScript: make install DESTDIR=%d
DocFiles: AUTHORS COPYING ChangeLog NEWS THANKS TODO
PostInstScript: <<
update-mime-database %p/share/mime >/dev/null
update-desktop-database -q %p/share/applications
<<
PostRmScript: <<  
update-mime-database %p/share/mime >/dev/null
update-desktop-database -q %p/share/applications
<<
Homepage: http://www.vips.ecs.soton.ac.uk
DescDetail: <<
nip2 is a graphical front end to the VIPS package. 

nip2 aims to be about halfway between Excel and Photoshop. You don't 
directly edit images --- instead, like a spreadsheet, you build 
relationships between objects. 

With nip2, rather than directly editing images, you build relationships 
between objects in a spreadsheet-like fashion. When you make a change 
somewhere, nip2 recalculates the objects affected by that change. Since it 
is demand-driven this update is very fast, even for very, very large 
images. nip2 is very good at creating pipelines of image manipulation 
operations. It is not very good for image editing tasks like touching up 
photographs. For that, a tool like the GIMP should be used instead.
<<
