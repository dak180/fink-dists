Package: gmerlin-avdecoder1
Version: 1.2.0
Revision: 3
###
Source:  mirror:sourceforge:gmerlin/gmerlin-avdecoder-%v.tar.gz
Source-MD5: 37b19266b098d9d05bb05ebef138ffbd
#Source-SHA1: 3754d4e6bac24c09e3d335100ba01f231e440dc5
###
DocFiles: README NEWS COPYING AUTHORS INSTALL ChangeLog
###
Maintainer: None <fink-devel@lists.sourceforge.net>
HomePage: http://gmerlin.sourceforge.net/
License: GPL 
Description: Library for media file decoding support
###
DescDetail: <<
This is gmerlin_avdecoder, a multimedia decoding library.
It it primarly a support library for gmerlin, but it can also be
used as a standalone library for getting sophisticated media file
decoding support for your application.
<<
#
# gmerlin-avdecoder1=1.2.0 doesn't work with ffmpeg-2.0
Depends: <<
	%N-shlibs (= %v-%r),
	a52dec-shlibs (>=0.7.4-2),
	faac-shlibs,
	libavcodec54-1.2-shlibs,
	libavformat54-1.2-shlibs,
	libcdio13-shlibs,
	libdca-shlibs,
	libdvdread.3-shlibs (>=0.9.5-1),
	libfaad2-shlibs,
	libflac8 (>=1.2.1-1000),
	libgavl1-shlibs (>=1.4.0-1),
	libgettext8-shlibs (>=0.17-1),
	libgnugetopt-shlibs,
	libiconv,
	libjpeg9-shlibs,
	libmad-shlibs (>=0.15.1b-5),
	libmpcdec5-shlibs (>=1.2.6-1),
	libogg-shlibs,
	liboil-0.3-shlibs,
	libopenjpeg1-shlibs,
	libpng16-shlibs,
	libpostproc52-1.2-shlibs,
	libschroedinger-shlibs,
	libspeex1-shlibs,
	libswscale2-1.2-shlibs,
	libtheora0-shlibs (>=1.0-0.alpha5.100),
	libtiff5-shlibs,
	libvorbis0-shlibs (>=1.2.0-1),
	xvidcore-shlibs
<<
#
BuildDepends: <<
	a52dec-dev (>=0.7.4-2),
	autoconf2.6,
	automake1.14,
	doxygen,
	faac-dev,
	fink-package-precedence,
	gettext-tools (>=0.17-1),
	libavcodec54-1.2-dev,
	libpostproc52-1.2-dev,
	libavformat54-1.2-dev,
	libavutil52-1.2-dev,
	libcdio13-dev,
	libdca-dev,
	libdvdread.3 (>=0.9.5-1),
	libfaad2-dev,
	libflac8-dev (>=1.2.1-1000),
	libgavl1 (>=1.4.0-1),
	libgettext8-dev (>=0.17-1),
	libgnugetopt,
	libiconv-dev,
	libjpeg9,
	libmad (>=0.15.1b-5),
	libmpcdec5 (>=1.2.6-1),
	libogg,
	liboil-0.3,
	libopenjpeg1,
	libpng16,
	libpostproc52-1.2-dev,
	libschroedinger,
	libspeex1,
	libswscale2-1.2-dev,
	libtheora0 (>=1.0-0.alpha5.100),
	libtiff5,
	libtool2,
	libvorbis0 (>=1.2.0-1),
	pkgconfig,
	xvidcore
<<
BuildDependsOnly: true
#
# don't rely on png.h to #include what is needed directly -- dmacks
# get libopenjpeg flags published via pkg-config instead of guessing --dmacks 
PatchFile: %n.patch
PatchFile-MD5: 592a3d3f1bdaec39a7f6a15431d94dd4
#
# we are currently not using libmpeg2 and mjpegtools because we think FFMPEG
# provides that stuff, no? And gmerlin isn't packaged currently.
#   --enable-libdca and --enable-theora should exist but do not, only the --disable versions
ConfigureParams: <<
  --disable-gmerlin \
  --enable-libavcodec \
  --enable-libpostproc \
  --enable-libswscale \
  --enable-libavformat \
  --enable-schroedinger \
  --enable-speex \
  --disable-mjpegtools \
  --enable-ogg \
  --enable-vorbis \
  --disable-libmpeg2 \
  --enable-libtiff \
  --enable-openjpeg \
  --disable-samba \
  --enable-libpng \
  --enable-faad2 \
  --enable-dvdread \
  --enable-flac \
  --enable-musepack \
  --enable-mad \
  --enable-liba52 \
  --enable-libcdio \
  --disable-win32 \
  --with-avcodec=%p/lib/ffmpeg-1.2 \
  --with-avformat=%p/lib/ffmpeg-1.2 \
  --with-vorbis=%p \
  --with-faad2-prefix=%p \
  --enable-dependency-tracking \
  AVCODEC_CFLAGS="-I%p/lib/ffmpeg-1.2/include" \
  AVCODEC_LIBS="-L%p/lib/ffmpeg-1.2/lib" \
  LIBPOSTPROC_CFLAGS="-I%p/lib/ffmpeg-1.2/include" \
  LIBPOSTPROC_LIBS="-L%p/lib/ffmpeg-1.2/lib" \
  LIBSWSCALE_CFLAGS="-I%p/lib/ffmpeg-1.2/include" \
  LIBSWSCALE_LIBS="-L%p/lib/ffmpeg-1.2/lib"
<<
# Arg! On 10.4, the LDFLAGS seem to be necessary, but not on 10.5. There is
# most likely a better solution for this.  These are all frameworks that are
# used by libcdio and are included in the CDIO_LIBS var.  Chatted briefly with
# dmacks and akh on #fink about this, but no luck.  It seems like it is
# related to 'pkg-config --libs libcdio' giving these frameworks on 10.4 but
# not on 10.5.
CompileScript: <<
./autogen.sh
LIBRARY_PATH="%p/lib:$LIBRARY_PATH" ./configure %c
make LDFLAGS="$LDFLAGS -Wl,-framework,Carbon -Wl,-framework,IOKit -Wl,-framework,DiskArbitration -Wl,-headerpad_max_install_names"
fink-package-precedence --prohibit-bdep=%n .
<<
###
SplitOff: <<
    Package: %n-shlibs
    Description: Shared libraries for gmerlin-avdecoder
    Depends: <<
		a52dec-shlibs (>=0.7.4-2),
		faac-shlibs,
		libavcodec54-1.2-shlibs,
		libavformat54-1.2-shlibs,
		libcdio13-shlibs,
		libdca-shlibs,
		libdvdread.3-shlibs (>=0.9.5-1),
		libfaad2-shlibs,
		libflac8 (>=1.2.1-1000),
		libgavl1-shlibs (>=1.4.0-1),
		libgettext8-shlibs (>=0.17-1),
		libgnugetopt-shlibs,
		libiconv,
		libjpeg9-shlibs,
		libmad-shlibs (>=0.15.1b-5),
		libmpcdec5-shlibs (>=1.2.6-1),
		libogg-shlibs,
		liboil-0.3-shlibs,
		libopenjpeg1-shlibs,
		libpng16-shlibs,
		libpostproc52-1.2-shlibs,
		libschroedinger-shlibs,
		libspeex1-shlibs,
		libswscale2-1.2-shlibs,
		libtheora0-shlibs (>=1.0-0.alpha5.100),
		libtiff5-shlibs,
		libvorbis0-shlibs (>=1.2.0-1),
		xvidcore-shlibs
	<<
    Files: <<
		lib/libgmerlin_avdec.1.dylib 
	<<
    Shlibs: %p/lib/libgmerlin_avdec.1.dylib 2.0.0 %n (>= 1.0.0-1)
    DocFiles: README NEWS COPYING AUTHORS INSTALL ChangeLog
<<
# the only file for bin is bgavdump which just gives info on the codecs and is
# mostly there as an example of how to use the API.  Seems like it should just
# be part of the main package
