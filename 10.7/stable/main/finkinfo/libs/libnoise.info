Package: libnoise
Version: 1.0.0
Revision: 1
Maintainer: Sjors Gielen <fink-libnoise@sjorsgielen.nl>

Description: Coherent noise-generating C++ library
BuildDependsOnly: true
Depends: libnoise-shlibs
BuildDepends: libtool2

License: LGPL2
Source: mirror:sourceforge:libnoise/libnoise%%20sources/%v/libnoisesrc-%v.zip
SourceDirectory: noise
Source-MD5: fc0d9b4f6477397568c3a9d5294b3b8c

PatchFile: libnoise.patch
PatchFile-MD5: 4332ec35262026d41906a960db03b075
PatchScript: <<
	%{default_script}
	### glibtool can't infer the tag.
	### Add here vs in PatchFile because source is full of DOS line endings.
	perl -pi -e 's|mode\=compile \$\(CXX\)|mode=compile --tag CXX \$\(CXX\)|g' src/Makefile
	perl -pi -e 's|mode\=compile \$\(CC\)|mode=compile --tag CC \$\(CC\)|g' src/Makefile
	perl -pi -e 's|mode\=link \$\(CXX\)|mode=link --tag CXX \$\(CXX\)|g' src/Makefile
	perl -pi -e 's|mode\=link \$\(CC\)|mode=link --tag CC \$\(CC\)|g' src/Makefile
<<
UseMaxBuildJobs: false
CompileScript: <<
  touch src/libnoise.a
  CXXFLAGS='-O3' make || true
  touch src/libnoise.a
  CXXFLAGS='-O3' make
  cp src/.libs/libnoise.a lib/
  install_name_tool -id %p/lib/libnoise.0.dylib lib/libnoise.0.3.dylib
<<

InstallScript: <<
  rm include/Makefile
  rm lib/Makefile
  mkdir -p %i/include
  mv include %i/include/noise
  mv lib %i
  ln -s libnoise.0.3.dylib %i/lib/libnoise.0.dylib
  ln -s libnoise.0.3.dylib %i/lib/libnoise.dylib
  mkdir -p %i/share/doc/libnoise
  mv doc/html %i/share/doc/libnoise
<<

SplitOff: <<
  Package: %n-shlibs
  Files: lib/libnoise.0.dylib lib/libnoise.0.3.dylib
  Shlibs: <<
    %p/lib/libnoise.0.dylib 0.0.0 %n (>= 1.0.0-1)
  <<
<<
Homepage: http://libnoise.sourceforge.net/
