Info4: <<
Package: qt5-mac-qtmultimedia
Version: 5.3.2
Revision: 1
Distribution: 10.9, 10.10
Description: Cross-Platform framework (%type_pkg[qt])
Type: qt (mac)
License: LGPL
Maintainer: None <fink-devel@lists.sourceforge.net>
### First version 5.3.1 by Hanspeter Niederstrasser
### Free to upgrade and modify parameters, but please discuss
### first on the fink-devel list

Depends: <<
	qt5-%type_pkg[qt]-qtmultimedia-shlibs (= %v-%r),
	qt5-%type_pkg[qt]-qtmultimediaquick-p-shlibs (= %v-%r),
	qt5-%type_pkg[qt]-qtmultimediawidgets-shlibs (= %v-%r)
<<
BuildDepends: <<
	qt5-%type_pkg[qt]-qtbase-dev-tools (>= %v-1),
	qt5-%type_pkg[qt]-qtbase (>= %v-1),
	qt5-%type_pkg[qt]-qtdeclarative (>= %v-1),
	fink (>= 0.34.4-1),
	fink-buildenv-modules,
	fink-package-precedence,
	pkgconfig,
	xcode (>= 5.1.1),
	xcode.app (>= 5.1.1)
<<
BuildConflicts: uuid, libevent1.4, libevent2
BuildDependsOnly: true
Source: http://download.qt-project.org/official_releases/qt/5.3/%v/submodules/qtmultimedia-opensource-src-%v.tar.xz
Source-MD5: 5ef68e85b9d32865a4bf7f491ff31f96
PatchFile: %n.patch
PatchFile-MD5: d5f97200fdd5f72d362c021592f01d58

NoSetCPPFLAGS: true
NoSetCFLAGS: true
NoSetCXXFLAGS: true

GCC: 4.0
CompileScript: <<
	#!/bin/sh -ev
	. %p/sbin/fink-buildenv-helper.sh
	export QT_FINK_PREFIX=%p/lib/qt5-%type_pkg[qt]
	#export CPATH=%p/include/dbus-1.0:%p/lib/dbus-1.0/include:%p/include
	#export LIBRARY_PATH=%p/lib
	export PATH=${QT_FINK_PREFIX}/bin:$PATH
	export PKG_CONFIG_PATH=%p/lib/glib-2.0/pkgconfig-strict:$PKG_CONFIG_PATH
	### need explicit PKG_CONFIG to detect dbus/sqlite3
	#export PKG_CONFIG=%p/bin/pkg-config
	qmake
	make
	fink-package-precedence --depfile-ext='\.d' --prohibit-bdep=%N .
<<

InstallScript: <<
	#!/bin/sh -ev
	export QT_FINK_PREFIX=%p/lib/qt5-%type_pkg[qt]

	make install INSTALL_ROOT=%d

	### Make sure we have all the right packages (by probing .pc files)
	pushd %d/${QT_FINK_PREFIX}/lib/pkgconfig
		### keep <space> at end of the 'want' list of .pc files
		want="Qt5Multimedia.pc Qt5MultimediaQuick_p.pc Qt5MultimediaWidgets.pc "
		have=`/bin/ls -1 | tr '\n' ' '`
	popd
	if [ "$want" != "$have" ]; then
		echo "Unexpected build results (mismatched list of .pc)"
		echo "  want: '$want'"
		echo "  have: '$have'"
		exit 1
	fi

	### Clean up .la, .prl files
	### remove build-dir location and fix '-framework ' -> '-Wl,-framework,'
	find %d/${QT_FINK_PREFIX}/lib -name \*.prl -o -name \*.la | xargs \
		perl -pi -e 's|%b/lib|%p/lib|g; s|-framework |-Wl,-framework,|g'

	### Clean up .pc files
	### fix '-framework ' -> '-Wl,-framework,'
	find %d/${QT_FINK_PREFIX}/lib -name \*.pc | xargs \
		perl -pi -e 's|-framework |-Wl,-framework,|g'
	
	### clean up Libs.private
	perl -ni -e 'print unless /Libs.private:/' %d/${QT_FINK_PREFIX}/lib/pkgconfig/*.pc
<<
#AppBundles: 
DocFiles: LICENSE.GPL LICENSE.LGPL LGPL_EXCEPTION.txt

SplitOff: <<
	Package: qt5-%type_pkg[qt]-qtmultimedia-shlibs
	Description: Qt multimedia library (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/lib/QtMultimedia.framework/Versions/5/QtMultimedia
	<<
	Shlibs: <<
		%p/lib/qt5-%type_pkg[qt]/lib/QtMultimedia.framework/Versions/5/QtMultimedia       5.3.0 %n (>= 5.3.1-1)
	<<
	DescDetail: The QtMultimedia module provides low-level multimedia functionality.
<<
SplitOff2: <<
	Package: qt5-%type_pkg[qt]-qtmultimediaquick-p-shlibs
	Description: Qt MultimediaQuick library (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimedia-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtqml-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtquick-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/lib/QtMultimediaQuick_p.framework/Versions/5/QtMultimediaQuick_p
	<<
	Shlibs: <<
		%p/lib/qt5-%type_pkg[qt]/lib/QtMultimediaQuick_p.framework/Versions/5/QtMultimediaQuick_p       5.3.0 %n (>= 5.3.1-1)
	<<
	DescDetail: 
<<
SplitOff3: <<
	Package: qt5-%type_pkg[qt]-qtmultimediawidgets-shlibs
	Description: Qt MultimediaWidgets library (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimedia-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtopengl-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtwidgets-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/lib/QtMultimediaWidgets.framework/Versions/5/QtMultimediaWidgets
	<<
	Shlibs: <<
		%p/lib/qt5-%type_pkg[qt]/lib/QtMultimediaWidgets.framework/Versions/5/QtMultimediaWidgets       5.3.0 %n (>= 5.3.1-1)
	<<
	DescDetail: 
<<
SplitOff4: <<
	Package: qt5-%type_pkg[qt]-multimedia-plugins
	Description: Qt5 multimedia plugins (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimedia-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimediawidgets-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtopengl-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtwidgets-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/plugins/audio
		lib/qt5-%type_pkg[qt]/plugins/mediaservice
		lib/qt5-%type_pkg[qt]/plugins/playlistformats
	<<
	DescDetail: 
<<
SplitOff5: <<
	Package: qt5-%type_pkg[qt]-qml-module-qtaudioengine
	Description: Qt5 audioengine QML module (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimedia-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtqml-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtquick-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/share/qt5/qml/QtAudioEngine
	<<
	DescDetail: 
<<
SplitOff6: <<
	Package: qt5-%type_pkg[qt]-qml-module-qtmultimedia
	Description: Qt5 multimedia QML module (%type_pkg[qt])
	Depends: <<
		qt5-%type_pkg[qt]-qtcore-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtgui-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimedia-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtmultimediaquick-p-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtnetwork-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtqml-shlibs (>= %v-%r),
		qt5-%type_pkg[qt]-qtquick-shlibs (>= %v-%r)
	<<
	Files: <<
		lib/qt5-%type_pkg[qt]/share/qt5/qml/QtMultimedia
	<<
	DescDetail: 
<<
Homepage: http://qt-project.org
DescDetail: <<
Qt is a cross-platform application and UI framework with APIs for
C++ programming and Qt Quick for rapid UI creation.

* Intuitive class libraries
* Easy to use and learn
* Produce highly readable, easily maintainable and reusable code
* High runtime performance and small footprint
<<
DescUsage: <<
To compile against this Qt5, you need to make sure that 
"%p/lib/qt5-%type_pkg[qt]/bin" is first in your PATH and that qmake is 
present. Qmake is provided by "qt5-%type_pkg[qt]-qtbase-dev-tools".

If you need to manually find the headers and libraries, you
need your compiler flags to contain: 

	-F%p/lib/qt5-%type_pkg[qt]/lib

	or this:

	-I%p/lib/qt5-%type_pkg[qt]/include

...and your linker flags to contain:

	-F%p/lib/qt5-%type_pkg[qt]/lib

	or

	-L%p/lib/qt5-%type_pkg[qt]/lib

In this case, it's still a good idea to set your PATH to contain
"%p/lib/qt5-%type_pkg[qt]/bin" as well.
<<
DescPackaging: <<
We follow Ubuntu's lead in using the separate tarballs and separate most
of the packages the same way they do.
<<
DescPort: <<
List of patched files and the reasoning
---------------------------------------
qtmultimedia/src/plugins/avfoundation/mediaplayer/avfmediaplayersession.mm
http://bugreports.qt-project.org/browse/QTBUG-41136
<<
<<
# End Info4
