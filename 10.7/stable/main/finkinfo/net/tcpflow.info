Package: tcpflow
Version: 1.4.4
Revision: 1
Description: Captures data transmitted in TCP connections
License: GPL
BuildDepends: <<
	boost1.53.nopython,
	bzip2-dev,
	cairo,
	expat1,
	fink (>= 0.32),
	fontconfig2-dev,
	freetype219,
	libpcap1,
	pixman,
	sqlite3-dev,
	system-openssl-dev
<<
Depends: <<
	bzip2-shlibs,
	cairo-shlibs,
	expat1-shlibs,
	fontconfig2-shlibs,
	freetype219-shlibs,
	libpcap1-shlibs,
	pixman-shlibs,
	sqlite3-shlibs
<<
Source: http://www.digitalcorpora.org/downloads/tcpflow/%n-%v.tar.gz
Source-MD5: f395fea6f5fe136543f4c982beff9cba
GCC: 4.0
ConfigureParams: --mandir='${prefix}/share/man'
InfoTest: <<
	TestScript: make check || exit 2
<<
InstallScript: make install DESTDIR=%d
Homepage: http://github.com/simsong/tcpflow/wiki
Maintainer: Nick Siripipat <nsiripip+fink@gmail.com>
DescDetail: <<
tcpflow is a program that captures data transmitted as part of TCP
connections (flows), and stores the data in a way that is convenient for
protocol analysis or debugging. A program like 'tcpdump' shows a summary of
packets seen on the wire, but usually doesn't store the data that's actually
being transmitted. In contrast, tcpflow reconstructs the actual data streams
and stores each flow in a separate file for later analysis. 

tcpflow understands sequence numbers and will correctly reconstruct data
streams regardless of retransmissions or out-of-order delivery. However,
it currently does not understand IP fragments; flows containing IP fragments
will not be recorded properly. 
<<
DocFiles: README AUTHORS COPYING NEWS
