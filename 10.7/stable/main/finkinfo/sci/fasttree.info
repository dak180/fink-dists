Package: fasttree
Version: 2.1.7
Revision: 2
Description: Fast inference of phylogenetic trees
License: GPL
Maintainer: Hanspeter Niederstrasser <nieder@users.sourceforge.net>
Depends: <<
	gcc49-shlibs
<<
BuildDepends: <<
	gcc49-compiler
<<
Source: http://www.microbesonline.org/fasttree/FastTree-%v.c
NoSourceDirectory: true
Source-MD5: 209c15343a878f2b23c44cd326a094e1
Source-Checksum: SHA1(d9381924829e7d19d56154ebbde0e44044b4b7ab)
CompileScript: <<
gcc-fsf-4.9 -O3 -finline-functions -funroll-loops -Wall -o FastTree FastTree-%v.c -lm
gcc-fsf-4.9 -DOPENMP -fopenmp -O3 -finline-functions -funroll-loops -Wall -o FastTreeMP FastTree-%v.c -lm
<<
InstallScript: <<
	mkdir %i/bin
	cp FastTree FastTreeMP %i/bin
	head -n 40 FastTree-%v.c > LICENSE
	head -n 285 FastTree-%v.c | tail -n 245 > README
<<
DocFiles: LICENSE README 
Homepage: http://www.microbesonline.org/fasttree/
DescDetail: <<
FastTree infers approximately-maximum-likelihood phylogenetic trees 
from alignments of nucleotide or protein sequences. FastTree can handle 
alignments with up to a million of sequences in a reasonable amount of 
time and memory. For large alignments, FastTree is 100-1,000 times 
faster than PhyML 3.0 or RAxML 7.

Price, M.N., Dehal, P.S., and Arkin, A.P. (2009) FastTree: Computing 
Large Minimum-Evolution Trees with Profiles instead of a Distance 
Matrix. Molecular Biology and Evolution 26:1641-1650, 
doi:10.1093/molbev/msp077.
<<
