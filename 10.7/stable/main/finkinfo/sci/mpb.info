Package: mpb
Version: 1.4.2
Revision: 1030
Maintainer: Aurelien Chanudet <aurelien.chanudet@m4x.org>
GCC: 4.0
Depends: <<
	fftw-shlibs,
	gcc48-shlibs,
	gmp5-shlibs,
	guile18-libs,
	guile18-shlibs,
	hdf5.8-shlibs,
	libtool2-shlibs
<<
BuildDepends: <<
	fftw,
	fink-package-precedence,
	gcc48-compiler,
	gmp5,
	guile18-dev,
	hdf5.8,
	libctl,
	libtool2
<<
BuildConflicts: g95, g77
Source: http://ab-initio.mit.edu/mpb/%n-%v.tar.gz
Source-MD5: e1e618b0db343a7a3fc38eabd69d008b
PatchFile: %n.patch
PatchFile-MD5: e0496d1a388ca1972a8c6df5875eefe1
UseMaxBuildJobs: false
SetCPPFLAGS: -DH5_USE_16_API=1 -MD
ConfigureParams: <<
	--mandir=%i/share/man \
	--with-libctl=%p/share/libctl \
	--with-blas="-framework vecLib" \
	--with-lapack="-framework vecLib" \
	F77=gfortran-fsf-4.8 \
	CC=gcc-fsf-4.8 \
	FLIBS='-lgfortranbegin -lgfortran'
<<
CompileScript: <<
#!/bin/sh -ev
 export PATH=%p/share/guile/1.8/scripts/binoverride:$PATH
 ./configure %c
 make
 fink-package-precedence --depfile-ext='\.d' .
<<
InfoTest: <<
	TestScript: make check || exit 2
<<
DocFiles: AUTHORS COPYING COPYRIGHT ChangeLog NEWS README doc mpb-ctl/examples
Description: Compute band structures of photonic crystals
DescDetail: <<
This program computes definite-frequency eigenstates of Maxwell's
equations in periodic dielectric structures for arbitrary wavevectors,
using fully-vectorial and three-dimensional methods. It is especially
designed for the study of photonic crystals (a.k.a. photonic band-gap
materials), but is also applicable to many other problems in optics,
such as waveguides and resonator systems. It was written by Steven G.
Johnson.
<<
DescPort: <<
- Set CPPFLAGS=-DH5_USE_16_API=1 in order to work with hdf5.8 (not -oldapi) as suggested in the mpb site
- Cannot use fftw3 (as of v1.4.2)
<<
Homepage: http://ab-initio.mit.edu/mpb/
License: GPL
