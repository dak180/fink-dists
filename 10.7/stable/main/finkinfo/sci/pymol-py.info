Info2: <<
Package: pymol-py%type_pkg[python]
Version: 1.7.4.0
Revision: 5
UseMaxBuildJobs: false
# Maintainer: W. G. Scott <wgscott@users.sourceforge.net>
Maintainer: Jack Howarth <howarth.at.fink@gmail.com>
Type: python (2.7)
# r4105
Source: mirror:sourceforge:fink/pymol-%v-src.tar.bz2
#Source: http://scottlab.ucsc.edu/~wgscott/pymol/pymol-%v-src.tar.bz2
Source-MD5: cb202cd8e1fdb96cbe85364be22a7351
Source2: http://www.weizmann.ac.il/ISPC/eMovie.py
Source2-MD5: 832252d4cee1ba88d50a35681b5ecd4b
Source3: http://www.weizmann.ac.il/ISPC/eMovie_rigimol.inp
Source3-MD5: 7f61fc224103aa24a92f1a03f985ce49
Source4: http://diablo.ucsc.edu/~wgscott/pymol/pynmr_0.37f_src.tar.bz2
Source4-MD5: cb4a3906766681ce9230ef5515e1aa1c
PatchFile: pymol-py.patch
PatchFile-MD5: ea11b72a2babced8e3c7274e9c0b5439
PatchFile2: pymol-py-clang.patch
PatchFile2-MD5: 2f5a13c81a56d37447a02fcc5b41ab7c
PatchFile3: pymol-py-rest.patch
PatchFile3-MD5: 2df10a5a2d66b16082317a823878bde0
PatchFile4: pymol-py-numpy.patch
PatchFile4-MD5: 270ace2c985cb87bdcaa2c1a06da5edf
PatchFile5: pymol-py-gl.patch
Patchfile5-MD5: b04a0b02ad38eddc3d3ad6a05ed69462
SourceDirectory: pymol-%v
# Avoid including headers from fink before source directories
NoSetCPPFLAGS: true
SetCFLAGS: -DNPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION -Wno-unused-variable -Wno-unused-label
Depends: python%type_pkg[python], libpng16-shlibs, scipy-py%type_pkg[python], tcltk, pmw-py%type_pkg[python] (>= 1.3.2-1000), freeglut-shlibs, glew1.11-shlibs, blt-shlibs, meschach-shlibs, freetype219-shlibs, libxml2-shlibs
BuildDepends: libpng16, freeglut, glew1.11, readline6, gdbm3, tcltk-dev, blt, x11-dev, swig, meschach, freetype219, libxml2, fink-buildenv-modules, fink (>= 0.30.0)
Conflicts: pymol, pymol-py22, pymol-py23, pymol-py24, pymol-py25, pymol-py26, pymol-py27
Replaces: pymol, pymol-py22, pymol-py23, pymol-py24, pymol-py25, pymol-py26, pymol-py27
PatchScript: <<
#! /bin/sh -ex
. %p/sbin/fink-buildenv-helper.sh
sed 's|@FINKPREFIX@|%p|g' < %{PatchFile} | sed 's|python2.X|python%type_raw[python]|g' | patch -p1
patch -p1 < %{PatchFile2}
patch -p1 < %{PatchFile4}
if [ $DARWIN_MAJOR_VERSION -ge 12 ] 
	then patch -p1 < %{PatchFile5}
fi
cd ..
sed 's|@FINKPREFIX@|%p|g' < %{PatchFile3} | sed 's|python2.X|python%type_raw[python]|g' | sed 's|pymol-py2X|pymol-py%type_pkg[python]|g' | patch -p1
cd pymol-%v
perl -pi -e 's|searchDirs.append\(os.path.join\("/opt", "local", "bin"\)\)||g' modules/pmg_tk/startup/apbs_tools.py 
<<
CompileScript: <<
#! /bin/sh -ex
if [ %type_raw[python] == 2.6 ]; then
  cd %b/../pynmr_0.37f_src/pmg_tk/startup/pynmr
  perl -pi -e 's|Numeric import|numpy.oldnumeric import|g' *.py
  perl -pi -e 's|LinearAlgebra import|numpy.oldnumeric.linear_algebra import|g' *.py
  perl -pi -e 's|import MLab|import numpy.oldnumeric.mlab as MLab|g' *.py
  perl -pi -e 's|main|int main|g' Crmsd.c
  ./run.macos
fi
<<
InstallScript: <<
#! /bin/sh -ex
%p/bin/python%type_raw[python] setup.py install --root %d
# setup2.py currently mishandles the creation of the pymol_path symlink
# and the placement of the associated subdirectories.
#%p/bin/python%type_raw[python] setup2.py install --root %d
#ln -s %p/lib/python%type_raw[python]/site-packages/pymol %i/lib/python%type_raw[python]/site-packages/pymol/pymol_path
cp -r data modules examples test scripts %i/lib/python%type_raw[python]/site-packages/pymol
mkdir -p %i/lib/pymol-py%type_pkg[python]/bin
cp pymol.cmd %i/lib/pymol-py%type_pkg[python]/bin/pymol
chmod ugo+x %i/lib/pymol-py%type_pkg[python]/bin/pymol
cp ../eMovie.py %i/lib/python%type_raw[python]/site-packages/pmg_tk/startup/eMovie.py
cp ../eMovie_rigimol.inp %i/lib/python%type_raw[python]/site-packages/pymol/eMovie_rigimol.inp
mkdir %i/lib/python%type_raw[python]/site-packages/pmg_tk/startup/pynmr
if [ %type_raw[python] == 2.6 ]; then
  cd ../pynmr_0.37f_src/pmg_tk/startup
  cp plug_nmr.py %i/lib/python%type_raw[python]/site-packages/pmg_tk/startup
  cd pynmr
  cp -r Crmsd.so *.py BIOPYTHON_LICENSE LICENSE README help images test_data %i/lib/python%type_raw[python]/site-packages/pmg_tk/startup/pynmr
  rm %i/lib/python%type_raw[python]/site-packages/pmg_tk/startup/pynmr/test_data/.noe.tbl.swp
fi
rm -f %i/bin/pymol
<<
DocFiles: DEVELOPERS LICENSE README
PostInstScript: <<
update-alternatives --install %p/bin/pymol pymol %p/lib/pymol-py%type_pkg[python]/bin/pymol %type_pkg[python]
<<
PreRmScript: <<
if [ $1 != "upgrade" ]; then
  update-alternatives --remove pymol %p/lib/pymol-py%type_pkg[python]/bin/pymol
fi
<<
Description: Molecular graphics system
DescDetail: <<
Launch with command "pymol"
Lauch with command "pymol -M" to avoid flickering artifacts on some ATI 
graphics cards.
Launch with command "pymol -S -t 6" for Zalman stereo.
PyMOL is a molecular graphics system with an embedded Python interpreter
designed for real-time visualization and rapid generation of high-quality
molecular graphics images and animations.
<<
DescPort: <<
Versions prior to 0.97-1 were ported and maintained by Matt Stephenson 
<cattrap@users.sourceforge.net>
<<
DescPackaging: <<
The tarball used for this version was created in accordance to the BSD
license of pymol using the following instructions...
   svn -r4105 co https://svn.code.sf.net/p/pymol/code/trunk/pymol pymol-1.7.4.0
   tar --exclude=.svn -jcvf pymol-1.7.4.0-src.tar.bz2 pymol-1.7.4.0
Added eMovie.py plug-in manually.  Commented out line that opens the window for eMovie by
default every time pymol is started as this is annoying behavior. eMovie is activated via 
the plugin menu.
Added  -d "_ set stereo_double_pump_mono,quiet=1" to exec on pymol to limit video
artifacts on drivers that do not properly support hardware stereo yet such as those for the
HD 2600 XT.
Added pynmr plugin.
<<
DescUsage: Just type 'pymol' at the command prompt inside an X environment.
License: OSI-Approved
Homepage: http://pymol.sourceforge.net
<<
